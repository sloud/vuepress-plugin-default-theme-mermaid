const { path } = require("@vuepress/utils");

function randomId() {
  return Math.random().toString(32).substr(2, 5).replace(/\s/, "");
}

function mermaidPlugin(md) {
  const temp = md.renderer.rules.fence.bind(md.renderer.rules);
  md.renderer.rules.fence = (tokens, idx, options, env, slf) => {
    const token = tokens[idx];
    const code = token.content.trim();

    if (token.info === "mermaid") {
      return `<MermaidWrapper dsl="${code}" uuid="${randomId()}"/>`;
    } else {
      return temp(tokens, idx, options, env, slf);
    }
  };
}

module.exports = {
  name: "mermaid-plugin",

  clientAppEnhanceFiles: [path.resolve(__dirname, "./clientAppEnhance.js")],

  extendsMarkdown: (md) => {
    md.use(mermaidPlugin);
  },
};
