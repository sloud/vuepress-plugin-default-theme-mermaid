import { defineClientAppEnhance } from "@vuepress/client"
import MermaidWrapper from "./MermaidWrapper.vue"

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.component("MermaidWrapper", MermaidWrapper);
});
