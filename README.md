# vuepress-plugin-default-theme-mermaid

**_Vuepress V2 plugin_**

This plugin allows you to write mermaid.js diagrams in your markdown like so

```mermaid
journey
    title My working day
    section Go to work
      Make tea: : Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

and have it rendered on the client side. The plugin will change the theme of
your diagrams based your vuepress theme. This is accomplished by watching for
the `dark` class on the root `html` element. This is how the default vuepress
theme swaps back and forth, but any other theme that uses a similar method
should also work.
